# quartoinrae

Repo pour construire une image docker avec r (rocker/verse) et l'extension [quarto INRAE](https://github.com/davidcarayon/quarto-inrae-extension), pour pouvoir générer en autre des documents quarto sans avoir besoin de réinstaller l'extension à chaque fois. 

Les images dockers sont dispos dans le container registry de ce repo et ont un tag par rapport à la version de R disponible. 

## Pris en charge dans ce container

- Les polices INRAE sont installées
- La compilation des pdf quarto avec mermaid demande un certain nombre de dépendancces linux qui sont résolues ici. 
- Dans les pdf inrae, il n'y a pas la cartouche "INRAE, Occitanie-Bordeaux" par défaut dans l'extension quarto INRAE.

## Exemple d'utilisation en CI/CD 

On peut remplacer :

```
image: rocker/verse:4.3.1

pages:
  before_script:
    - quarto install extension davidcarayon/quarto-inrae-extension --no-prompt
  script:
    - quarto render --output-dir public
  artifacts:
    paths:
      - public
```

avec quartoinrae :

```
image: registry.forgemia.inra.fr/elisemaigne/quartoinrae/quartoinrae:4.3.1

pages:
  script:
    - mkdir ./_extensions
    - cp -r /_extensions/* ./_extensions/
    - quarto render --output-dir public
  artifacts:
    paths:
      - public
```




