ARG R_VERSION

FROM rocker/verse:${R_VERSION}
# Make mermaid export work in pdf
RUN sudo apt-get update
RUN sudo apt-get install -yq libatk-bridge2.0-0 libcups2 libxkbcommon-x11-0 libxcomposite-dev libxdamage1 libxfixes3 libxrandr2 libgbm-dev libpangocairo-1.0-0 libgtk-3-0 libgtk-3-dev

# Install official INRAE polices
RUN curl https://charte-identitaire.intranet.inrae.fr/content/download/3007/30036?version=5 --output polices.zip
RUN unzip polices.zip
RUN unzip -j POLICES/Avenir\ Next\ Pro\ Complete\ Family\ Pack.zip -d $HOME/.fonts
RUN unzip -j POLICES/Raleway.zip -d $HOME/.fonts
# Install INRAE extension for quarto
RUN quarto install extension --no-prompt davidcarayon/quarto-inrae-extension@v1.0.0
RUN quarto tools install chromium

